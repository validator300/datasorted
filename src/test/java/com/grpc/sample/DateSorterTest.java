package com.grpc.sample;

import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class DateSorterTest {
    private final DateSorter dateSorter = new DateSorter();

    @Test
    void testSortDates() {
        List<LocalDate> unsortedDates = Arrays.asList(
                LocalDate.of(2004, 7, 1),
                LocalDate.of(2005, 1, 2),
                LocalDate.of(2007, 1, 1),
                LocalDate.of(2032, 5, 3)
        );

        Collection<LocalDate> sortedDates = dateSorter.sortDates(unsortedDates);

        List<LocalDate> expectedSortedDates = Arrays.asList(
                LocalDate.of(2005, 1, 2),
                LocalDate.of(2007, 1, 1),
                LocalDate.of(2032, 5, 3),
                LocalDate.of(2004, 7, 1)
        );

        assertEquals(expectedSortedDates, sortedDates);
    }

    @Test
    void testWithSameMonths(){
        List<LocalDate> unsortedDates = Arrays.asList(
                LocalDate.of(2004, 9, 1),
                LocalDate.of(2005, 9, 2),
                LocalDate.of(2007, 9, 1),
                LocalDate.of(2032, 9, 3)
        );

        Collection<LocalDate> sortedDates = dateSorter.sortDates(unsortedDates);

        List<LocalDate> expectedSortedDates = Arrays.asList(
                LocalDate.of(2004, 9, 1),
                LocalDate.of(2005, 9, 2),
                LocalDate.of(2007, 9, 1),
                LocalDate.of(2032, 9, 3)
        );

        assertEquals(expectedSortedDates, sortedDates);
    }

    @Test
    void testWithoutRInMonths(){
        List<LocalDate> unsortedDates = Arrays.asList(
                LocalDate.of(2004, 6, 1),
                LocalDate.of(2005, 6, 2),
                LocalDate.of(2007, 6, 1),
                LocalDate.of(2032, 6, 3)
        );

        Collection<LocalDate> sortedDates = dateSorter.sortDates(unsortedDates);

        List<LocalDate> expectedSortedDates = Arrays.asList(
                LocalDate.of(2032, 6, 3),
                LocalDate.of(2007, 6, 1),
                LocalDate.of(2005, 6, 2),
                LocalDate.of(2004, 6, 1)
        );

        assertEquals(expectedSortedDates, sortedDates);
    }

    @Test
    void testMonths(){
        List<LocalDate> unsortedDates = Arrays.asList(
                LocalDate.of(2001, 1, 1),
                LocalDate.of(2002, 2, 1),
                LocalDate.of(2003, 3, 1),
                LocalDate.of(2004, 4, 1),
                LocalDate.of(2005, 5, 1),
                LocalDate.of(2006, 6, 2),
                LocalDate.of(2007, 7, 1),
                LocalDate.of(2008, 8, 3),
                LocalDate.of(2009, 9, 3),
                LocalDate.of(2010, 10, 3),
                LocalDate.of(2011, 11, 3),
                LocalDate.of(2011, 12, 3)
        );

        Collection<LocalDate> sortedDates = dateSorter.sortDates(unsortedDates);

        List<LocalDate> expectedSortedDates = Arrays.asList(
                LocalDate.of(2001, 1, 1),
                LocalDate.of(2002, 2, 1),
                LocalDate.of(2003, 3, 1),
                LocalDate.of(2004, 4, 1),
                LocalDate.of(2009, 9, 3),
                LocalDate.of(2010, 10, 3),
                LocalDate.of(2011, 11, 3),
                LocalDate.of(2011, 12, 3),

                LocalDate.of(2008, 8, 3),
                LocalDate.of(2007, 7, 1),
                LocalDate.of(2006, 6, 2),
                LocalDate.of(2005, 5, 1)
        );

        assertEquals(expectedSortedDates, sortedDates);
    }
}